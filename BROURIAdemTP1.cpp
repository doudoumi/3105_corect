#include <iostream>
#include <string.h>
#include <map>
#include <vector>
#include <algorithm>
#include <set>

#define bruno_malenfant main
#define Valeur_Si_Introuvable -1
/*
*Nom:BROURI,Adem
*Code permanent:BRO23089702
*
*/

using namespace std;

class Element
{
  protected:
    string identificateur = "initial-String";
    int rang = -1;
    Element *suivant = nullptr;
    friend string Representant_De_class(Element *element, bool h);
    friend istream &operator>>(std::istream &is, Element &element);
    friend bool operator==(Element &a, Element &b);
    friend string Representant_commun(Element *element, Element *element1);
    friend void commande_traitement_rep_zero(string const &Line, bool h);
    friend void commande_traitement_rep(string const &Line, int rep);
    friend void commande_traitement_ce(string const &Line, int ce, bool h);
    friend void commande_traitement(string const &Line, bool h);
    //PS: cette classe a plus d'amis que moi .
  public:
    Element(string id = "VIDE", int ra = 0, Element *suiv = nullptr) : identificateur(id), rang(ra), suivant(suiv)
    {
    }
    Element(Element const &CopieAelment) : identificateur(CopieAelment.identificateur), rang(CopieAelment.rang), suivant(CopieAelment.suivant)
    {
    }
    virtual ~Element()
    {
        delete suivant;
    }
};

map<string, Element *> Element_map;

/*
*Vérifie si deux éléments sont égaux.
*
*@param &a Réfrence à un élément.
*@param &b Référence à un élément.
*@return   Un bollean.
*/
bool operator==(Element &a, Element &b)
{
    return a.identificateur == b.identificateur;
}
/*
*Cherche le représentant d'un élément.
*
*@param h un boolean dépandant de l'arguments -h.
*@param *element un pointeur vers un élément.
*@return identificateur l'identificateur du représentant de l'élément.
*/
string Representant_De_class(Element *element, bool h)
{
    string Representant, fouilleur;
    if (h)
    {
        while (element->suivant != nullptr)
        {
            element = element->suivant;
        }
        Representant = element->identificateur;
    }
    else
    {
        fouilleur = element->identificateur;
        map<string, Element *>::iterator it;
        it = Element_map.find(fouilleur);
        while (it->second->suivant != nullptr && it != Element_map.end())
        {
            fouilleur = it->second->suivant->identificateur;
            it = Element_map.find(fouilleur);
        }
        Representant = fouilleur;
    }
    return Representant;
}
/*
*Permet de recherche l'indentificateur commun le plus proche par deux méthode dépendament de l'argument -h.
*
*@param *element un pointeur vers un element.
*@param *element un pointeur vers un element.
*@return identificateur un string donnant l'identificateur commun le plus proche.
*
*PS:Désolé d'utilisé un algo O(n²).
*/
string Representant_commun(Element *element, Element *element1)
{
    map<string, Element *>::iterator it, it2;
    vector<string> tab_el, tab_el2;
    string Representant_commun = "null", fouilleur = element->identificateur, fouilleur2 = element1->identificateur;
    it = Element_map.find(fouilleur);
    it2 = Element_map.find(fouilleur2);
    while ((it->second->suivant != nullptr) && (it != Element_map.end()))
    {
        fouilleur = it->second->suivant->identificateur;
        it = Element_map.find(fouilleur);
        tab_el.push_back(fouilleur);
    }
    while ((it2->second->suivant != nullptr) && (it2 != Element_map.end()))
    {
        fouilleur2 = it2->second->suivant->identificateur;
        it2 = Element_map.find(fouilleur2);
        tab_el2.push_back(fouilleur2);
    }
    bool existe = false;
    for (int i = tab_el.size() - 1; i >= 0; --i)
    {
        for (int j = tab_el2.size() - 1; j >= 0; --j)
        {
            if (tab_el.at(i) == tab_el2.at(j))
            {
                existe = true;
                Representant_commun = tab_el.at(i);
            }
        }
    }
    if (!existe)
    {
        cerr << "Identificateur inexistant." << endl;
    }
    return Representant_commun;
}
/*
*Traite les cas d'entrees non conformes.
*
*@param i un int qui représente la fin supposé de la commande.
*@param Line une référence vers la ligne à traité. 
*/
void traitement_error(string const &Line, int i)
{
    bool Pas_un_Espace = false;
    for (unsigned int Char_increment = i + 1; Char_increment < Line.length(); ++Char_increment)
    {
        if (Line.at(Char_increment) != ' ')
        {
            Pas_un_Espace = true;
        }
    }
    if (Pas_un_Espace)
    {
        cerr << "Entrees non conforme." << endl;
        exit(-1);
    }
}
/*
*Traite la ligne si la ligne contient le string rep en début de ligne.
*
*@param h un boolean dépandant de l'arguments -h.
*@param Line une référence vers la ligne à traité.
*/
void commande_traitement_rep_zero(string const &Line, bool h)
{
    int i = 2;
    string id_Cherche;
    vector<string> Arg_tab;
    while (Line.at(i) != '?')
    {
        while (Line.at(i + 1) != ',' && Line.at(i + 1) != '?')
        {
            i = i + 1;
            id_Cherche.push_back(Line.at(i));
        }
        id_Cherche.erase(remove(id_Cherche.begin(), id_Cherche.end(), ' '), id_Cherche.end());
        Arg_tab.push_back(id_Cherche);
        id_Cherche.clear();
        i = i + 1;
    }
    if (Arg_tab.size() != 2)
    {
        cout << Representant_De_class(Element_map.at(Arg_tab.at(0)), h) << endl;
        Arg_tab.clear();
    }
    else
    {
        if (Representant_commun(Element_map.at(Arg_tab.at(0)), Element_map.at(Arg_tab.at(1))) != "null")
        {
            cout << Representant_commun(Element_map.at(Arg_tab.at(0)), Element_map.at(Arg_tab.at(1))) << endl;
        }
        Arg_tab.clear();
    }
    traitement_error(Line, i);
}
/*
*Traite la ligne si le string rep se trouve en milieu de ligne.
*
*@param rep un int qui donne la position de rep dans la ligne.
*@param Line une référence vers la ligne à traité.
*/

void commande_traitement_rep(string const &Line, int rep)
{
    int i = 2, j = 0;
    string idA, REP;
    static vector<string> Arg_tab;
    while (Line.at(rep + i) != '.')
    {
        while (Line.at(rep + i + 1) != ',' && Line.at(rep + i + 1) != '.')
        {
            i = i + 1;
            idA.push_back(Line.at(rep + i));
        }
        idA.erase(remove(idA.begin(), idA.end(), ' '), idA.end());
        Arg_tab.push_back(idA);
        idA.clear();
        i = i + 1;
    }

    while (j != rep)
    {
        REP.push_back(Line.at(j));
        ++j;
    }
    REP.erase(remove(REP.begin(), REP.end(), ' '), REP.end());
    map<string, Element *>::iterator it;
    for (unsigned int k = 0; k < Arg_tab.size(); ++k)
    {
        it = Element_map.find(Arg_tab.at(k));
        if (it == Element_map.end())
        {
            Element_map[Arg_tab.at(k)] = new Element(Arg_tab.at(k));
        }
        Element_map.at(Arg_tab.at(k))->suivant = Element_map.at(REP);
        if (Element_map.at(Arg_tab.at(k))->rang >= Element_map.at(REP)->rang)
        {
            Element_map.at(REP)->rang = Element_map.at(Arg_tab.at(k))->rang + 1;
        }
        else
        {
            Element_map.at(REP)->rang += 1;
        }
        traitement_error(Line, rep + i);
    }
    Arg_tab.clear();
}
/*
*Traite la ligne si le string ce se trouve dans la ligne.
*
*@param h un boolean dépandant de l'arguments -h.
*@param ce un int qui donne la position de ce dans la ligne.
*@param Line une référence vers la ligne à traité.
*/
void commande_traitement_ce(string const &Line, int ce, bool h)
{
    int i = 1, j = 0;
    string idB, CE;
    while (Line.at(ce + i + 1) != '.' && Line.at(ce + i + 1) != '?')
    {
        i = i + 1;
        idB.push_back(Line.at(ce + i));
    }
    idB.erase(remove(idB.begin(), idB.end(), ' '), idB.end());
    while (j != ce)
    {
        CE.push_back(Line.at(j));
        j = j + 1;
    }
    CE.erase(remove(CE.begin(), CE.end(), ' '), CE.end());
    if (Line.at(ce + i + 1) == '.')
    {
        if (Element_map.at(idB)->rang > Element_map.at(CE)->rang)
        {
            Element_map.at(CE)->suivant = Element_map.at(idB)->suivant;
        }
        else
        {
            Element_map.at(idB)->suivant = Element_map.at(CE)->suivant;
        }
    }
    else if (Line.at(ce + i + 1) == '?')
    {
        if (Representant_De_class(Element_map.at(idB), h) == Representant_De_class(Element_map.at(CE), h))
        {
            cout << "oui" << endl;
        }
        else
        {
            cout << "non" << endl;
        }
    }
    traitement_error(Line, ce + i + 1);
}
/*
*Redirige la ligne vers la fonction adapté à son traitement et gére la création d'éléments.
*
*@param h un boolean dépandant de l'arguments -h.
*@param Line une référence vers la ligne à traité.
*/
void commande_traitement(string const &Line, bool h)
{
    int rep = Line.find("rep");
    int ce = Line.find("ce");
    if (rep != Valeur_Si_Introuvable && rep != 0)
    {
        commande_traitement_rep(Line, rep);
    }
    else if (rep == 0)
    {
        commande_traitement_rep_zero(Line, h);
    }
    else if (ce != Valeur_Si_Introuvable)
    {
        commande_traitement_ce(Line, ce, h);
    }
    else
    {
        int i = 0;
        string idAdd;
        while (Line.at(i) != '.')
        {
            idAdd.push_back(Line.at(i));
            i = i + 1;
        }
        idAdd.erase(remove(idAdd.begin(), idAdd.end(), ' '), idAdd.end());
        map<string, Element *>::iterator it;
        it = Element_map.find(idAdd);
        if (it == Element_map.end())
        {
            Element_map[idAdd] = new Element(idAdd);
        }
    }
}
int bruno_malenfant(int argc, char *argv[])
{
    bool h = false;
    if (argc > 1)
    {
        string Test_argv = argv[1];
        if (Test_argv == "-h")
        {
            h = true;
        }
    }
    while (cin)
    {
        string Line;
        getline(cin, Line);
        commande_traitement(Line, h);
    }
    return 0;
}
